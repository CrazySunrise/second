import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class main {
    public static String Vvod(String message) {
        Scanner sc = new Scanner(System.in);
        System.out.println(message);
        return sc.next();
    }

    public static void main(String[] args) {
        List<biblio> biblios = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        int a = -2,b = 0,c = 0,d = 0,e = 0, f = 0, o = 0, drugsCount = 0, fruitsCount = 0, g;
        while (a != 0) {
            a = -2; b = 0; c = 0; d = 0; e = 0; f = 0; o = 0;
            System.out.println("\n0-Выход\n1-Добавление\n2-Удаление\n3-Просмотр\n4-Поиск");
            a = scanner.nextInt();
            switch (a){
                case 1:
                    biblios.add(new biblio(Vvod("Название"), Vvod("Автор"), Integer.valueOf(Vvod("Год издания"))));
                    break;
                case 2:
                    System.out.println("Введите название книги для удаления");
                    String delName = scanner.next();
                    for ( int i = 0; i < biblios.size(); i++ ){
                        if (biblios.get(i).getName().equals(delName)){
                            biblios.remove(i);
                        }
                    }
                    break;
                case 3:
                    for (biblio biblio : biblios){
                        biblio.printer();
                    }
                    break;
                case 4:
                    System.out.println("\nПоиск по:\n1-Названию\n2-Автору\n3-Году издания\n");
                    b = scanner.nextInt();
                    switch (b){
                        case 1:
                            System.out.println("Введите название книги");
                            String findName = scanner.next();
                            for (biblio bibliol : biblios){
                                if (bibliol.getName().equals(findName)){
                                    bibliol.printer();
                                }
                            }
                            break;
                            case 2:
                            System.out.println("Введите автора книги");
                            String findAuthor = scanner.next();
                                for (biblio bibliol : biblios){
                                    if (bibliol.getAuthor().equals(findAuthor)){
                                        bibliol.printer();
                                    }
                                }
                            break;
                            case 3:
                            System.out.println("Введите год издания книги");
                            int findYear = scanner.nextInt();
                                for (biblio bibliol : biblios){
                                    if (bibliol.getYearPubl() == findYear){
                                        bibliol.printer();
                                    }
                                }
                            break;
                    }
            }


    }
}
}
