public class biblio {
    String author;
    int yearPubl;
    String name;

    public biblio(String name, String author, int yearPubl){
        this.author = author;
        this.yearPubl = yearPubl;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getYearPubl() {
        return yearPubl;
    }

    public String getAuthor() {
        return author;
    }

    public void printer(){
        System.out.printf("\nНазвание: %s\nАвтор: %s\nГод издания: %d\n", name, author, yearPubl);
    }
}
